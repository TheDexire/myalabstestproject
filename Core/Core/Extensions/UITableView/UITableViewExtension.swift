//
//  UITableViewExtension.swift
//  Core
//
//  Created by Vitaliy Vassilyev on 30.03.2023.
//

import UIKit

public extension UITableView {
    func register(cellClass: AnyClass) {
        register(cellClass, forCellReuseIdentifier: "\(cellClass)")
    }
    
    func register(headerFooterClass: AnyClass) {
        register(headerFooterClass, forHeaderFooterViewReuseIdentifier: "\(headerFooterClass)")
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: "\(T.self)", for: indexPath) as? T else {
            fatalError("register(cellClass:) has not been implemented")
        }
        return cell
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: "\(T.self)") as? T else {
            fatalError("register(aClass:) has not been implemented")
        }
        return view
    }
}
