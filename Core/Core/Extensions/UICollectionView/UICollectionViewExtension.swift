//
//  UICollectionViewExtension.swift
//  Core
//
//  Created by Vitaliy Vassilyev on 30.03.2023.
//

import UIKit

public extension UICollectionView {
    func register(cellClass: AnyClass) {
        register(cellClass, forCellWithReuseIdentifier: "\(cellClass)")
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: "\(T.self)", for: indexPath) as? T else {
            fatalError("register(cellClass:) has not been implemented")
        }
        return cell
    }
}
