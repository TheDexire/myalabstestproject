//
//  UIColor+Hex.swift
//  Core
//
//  Created by Vitaliy Vassilyev on 30.03.2023.
//

import UIKit

public extension UIColor {
    convenience init?(hex: String) {
        guard hex.hasPrefix("#"),
              hex.count == 7,
              let hexNumber = UInt32(hex.dropFirst(), radix: 16)
        else { return nil }
        
        let r = CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0
        let g = CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0
        let b = CGFloat((hexNumber & 0x0000FF)) / 255.0
        let a = CGFloat(1.0)
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}
